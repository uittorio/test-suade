import { ChartViewProps } from "../chartView/ChartViewProps";

export interface AppState {
	showTable: boolean;
	chartLine?: ChartViewProps;
	chartPie?: ChartViewProps;
	selectedKey?: string;
}

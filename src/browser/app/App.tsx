import * as React from "react";
import './App.scss';
import { FileUpload } from "../fileUpload/FileUpload";
import { Csv } from "../../core/csv/Csv";
import { Table } from "../table/Table";
import { TableSales } from "../../core/tableSales/TableSales";
import { TableDataSetGroup } from "../../core/tableDataSet/TableDataSetGroup";
import { TableSalesStyle } from "../../core/tableSales/TableSalesStyle";
import { AppProps } from "./AppProps";
import { AppState } from "./AppState";
import { ChartView } from "../chartView/ChartView";
import { CsvSales } from "../../core/csvSales/CsvSales";

export class App extends React.Component<AppProps, AppState> {
	private csv: Csv;
	
	constructor(props: AppProps) {
		super(props);
		this.state = {
			showTable: false,
		};
	}
	
	render() {
		const table = this.state.showTable ? (
			<div>
      	<div className="App-table">
        	<Table headers={this.csv.headerAsArray()} row={this.csv.rowAsArray()} selectedKey={this.state.selectedKey} onValueChange={(newValue: string, keyValue: string, rowIndex: number) => this.updateAndShowTable(newValue, keyValue, rowIndex)}/>
      	</div>
				<div className="App-chartContainer">
					<ChartView type={this.state.chartLine.type} labels={this.state.chartLine.labels} datasets={this.state.chartLine.datasets} onSelected={this.state.chartLine.onSelected}/>
					<ChartView type={this.state.chartPie.type} labels={this.state.chartPie.labels} datasets={this.state.chartPie.datasets} onSelected={this.state.chartPie.onSelected}/>
				</div>
			</div>
		) : null;
		
		return (
			<div className="App">
				<div className="App-fileUpload">
					<FileUpload parser={this.props.injector.get('csvParser')} onUpload={csv => this.showTable(csv)}/>
				</div>
				{table}
		</div>)
	}
	
	public showTable(csv: Csv) {
		this.csv = new CsvSales(csv);
		
		const tableSales: TableSales = new TableSales(this.csv);
		
		this.setCharts(tableSales);
		
		this.setState({
			showTable: true
		});
	}
	
	public updateAndShowTable(newValue, rowKey, rowIndex) {
		this.csv.updateRow(rowIndex, rowKey, newValue);
		
		this.showTable(this.csv);
	}
	
	public setCharts(tableSales: TableSales) {
		this.setState({
			chartLine: {
				type: 'line',
				labels: Object.keys(tableSales.getYearsAmount()),
				datasets: tableSales.getYearsByContinentValues().map((value: TableDataSetGroup<number>) => {
					return {
						label: value.label,
						data: Object.values(value.values),
						backgroundColor: [TableSalesStyle.getBackgroundColor(value.label)]
					};
				}),
				onSelected: (index: number) => {
					this.setState({
						selectedKey: this.state.chartLine.labels[index]
					});
				}
			}
		});
		
		this.setState({
			chartPie: {
				type: 'pie',
				labels: Object.keys(tableSales.getContinentAmount()),
				datasets: [
					{
						label: '',
						data: Object.values(tableSales.getContinentAmount()),
						backgroundColor: TableSalesStyle.getAllBackgroundColor()
					}
				],
				onSelected: (index: number) => {
					this.setState({
						selectedKey: this.state.chartPie.labels[index]
					});
				}
			}
		})
	}
}

export default App;

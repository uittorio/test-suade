import { CsvFactory } from "../../core/csv/CsvFactory";
import { ParseResult } from "papaparse";
import { HtmlCsv } from "./HtmlCsv";

export class HtmlCsvFactory implements CsvFactory<ParseResult> {
	public create(data: ParseResult): HtmlCsv {
		return HtmlCsv.create(data);
	}
}
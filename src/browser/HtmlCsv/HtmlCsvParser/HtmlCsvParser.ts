import * as Papa from "papaparse";
import { CsvParser } from "../../../core/csv/CsvParser";
import { HtmlCsv } from "../HtmlCsv";
import { HtmlCsvFactory } from "../HtmlCsvFactory";
import { ParseResult } from "papaparse";

export class HtmlCsvParser implements CsvParser {
	private _csvFactory: HtmlCsvFactory;
	
	constructor(fileFactory: HtmlCsvFactory) {
		this._csvFactory = fileFactory;
	}
	
	public parse(fileData: File): Promise<HtmlCsv> {
		return new Promise((resolve: Function) => {
			Papa.parse(fileData, {
				complete: (results: ParseResult) =>  {
					return resolve(this._csvFactory.create(results));
				},
				header: true
			});
		});
	}
}

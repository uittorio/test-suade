import { ParseResult } from "papaparse";
import { Csv } from "../../core/csv/Csv";
import { TableHeader } from "../../core/table/TableHeader";
import { TableRowConfig } from "../../core/table/TableRowConfig";

export class HtmlCsv implements Csv {
	private _headers: { [key: string]: string } = {};
	private _rows: Array<{ [key: string]: string }> = [];
	
	public headerAsArray(): Array<TableHeader> {
		return Object.keys(this._headers).map((key: string) => {
			return {
				key,
				value: key
			}
		});
	}
	
	public header(): { [key: string]: string } {
		return this._headers;
	}
	
	public row(): Array<{ [key: string]: string }> {
		return this._rows;
	}
	
	public rowAsArray(): Array<Array<TableRowConfig>> {
		const rows: Array<Array<TableRowConfig>> = [];
		
		this._rows.forEach((dataRow) => {
			const row: Array<TableRowConfig> = [];
			
			Object.keys(dataRow).forEach((key: string) => {
				row.push({
					key,
					value: dataRow[key],
					editable: false
				})
			});
			
			rows.push(row);
		});
		
		return rows;
	}
	
	public updateRow(rowIndex: number, rowKey: string, newValue: string) {
		const row: { [key: string]: string } = this._rows.find((row: { [key: string]: string }, index: number) => {
			return index === rowIndex;
		});
		
		row[rowKey] = newValue;
	}
	
	private constructor(data: ParseResult) {
		data.meta.fields.forEach((field: string) => {
			this._headers[field] = field;
		});
		
		this._rows = data.data;
	}
	
	public static create(data: ParseResult) {
		return new HtmlCsv(data);
	}
}
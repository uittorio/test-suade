import * as React from "react";
import { TableHeader } from "../../core/table/TableHeader";
import { TableRowConfig } from "../../core/table/TableRowConfig";
import './Table.scss';
import { TableRow } from "./TableRow";

interface TableProp {
	headers: Array<TableHeader>;
	row: Array<Array<TableRowConfig>>;
	selectedKey: string;
	onValueChange(newValue: string, keyValue: string, rowIndex: number): void;
}
export class Table extends React.Component<TableProp> {
	constructor(props) {
		super(props);
	}
	render() {
		const headers: Array<JSX.Element> = [];
		const rows: Array<JSX.Element> = [];
		
		this.props.headers.forEach((header: TableHeader, key: number) => {
			headers.push(<td className="Table-headerRow" key={key}>{header.value}</td>);
		});
		
		this.props.row.forEach((row: Array<TableRowConfig>, rowIndex: number) => {
			const rowValues: Array<JSX.Element> = [];
			let isSelected: boolean = false;
			
			row.forEach((valueRow: TableRowConfig, index: number) => {
				if (valueRow.value === this.props.selectedKey) {
					isSelected = true;
				}
				
				rowValues.push(<TableRow key={index} value={valueRow.value} editable={valueRow.editable} onInputChange={(newValue) => this.props.onValueChange(newValue, valueRow.key, rowIndex)}/>)
			});
			
			const classNames: string = this._getRowGroupClasseName(isSelected);
			
			rows.push(<tr className={classNames} key={rowIndex}>{rowValues}</tr>);
		});
		
		return (
			<div className="Table">
				<table className="Table-container">
					<tbody>
					<tr className="Table-headerContainer">
						{headers}
					</tr>
					
					{rows}
					</tbody>
				</table>
			</div>)
	}
	
	private _getRowGroupClasseName(isSelected: boolean): string {
		return isSelected ? 'Table-rowGroup Table-rowGroupSelected' : 'Table-rowGroup';
	}

}
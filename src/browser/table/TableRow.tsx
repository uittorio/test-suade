import * as React from "react";

export interface TableRowProp {
	value: string;
	editable: boolean;
	onInputChange(value: string): void;
}

interface TableRowState {
	value: string;
}

export class TableRow extends React.Component<TableRowProp, TableRowState> {
	constructor(props) {
		super(props);
		this.state = {
			value: this.props.value
		}
	}
	render() {
		return (
			<td className="Table-row"><input type="text" value={this.state.value} readOnly={!this.props.editable} onChange={(e) => this.onInputChange(e)}/></td>
		)
	}
	
	private onInputChange(event: any) {
		this.setState({
			value: event.target.value
		});
		
		this.props.onInputChange(event.target.value);
	}
}
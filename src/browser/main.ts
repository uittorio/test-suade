import './main.scss';
import { CsvParser } from "../core/csv/CsvParser";
import { HtmlCsvParser } from "./HtmlCsv/HtmlCsvParser/HtmlCsvParser";
import { HtmlCsvFactory } from "./HtmlCsv/HtmlCsvFactory";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { SInjector } from "../core/injector/injector";
import App from "./app/App";

const injector: SInjector = new SInjector();
const csvFactory: HtmlCsvFactory = new HtmlCsvFactory();
const csvParser: CsvParser = new HtmlCsvParser(csvFactory);

injector.add('csvParser', csvParser);

ReactDOM.render(React.createElement(App, { injector: injector }), document.getElementById("app"));
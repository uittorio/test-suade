export interface ChartViewProps {
	type: string;
	labels: Array<string>;
	datasets: Array<{
		label: string;
		data: Array<number>;
		backgroundColor: Array<string>;
	}>
	onSelected(index: number): void;
}

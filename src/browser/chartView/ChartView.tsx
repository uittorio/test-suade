import * as React from "react";
import { ChartViewProps } from "./ChartViewProps";
import * as Chart from "chart.js";
import './ChartView.scss';

export class ChartView extends React.Component<ChartViewProps> {
	private chartRef: React.RefObject<any>;
	private chart: Chart;
	
	constructor(props) {
		super(props);
		this.chartRef = React.createRef();
	}
	
	public componentDidMount() {
		this.createChart(this.chartRef.current);
	}
	
	public componentDidUpdate() {
		this.updateChart(this.chartRef.current);
	}

	public render() {
		return (
			<div className="ChartView">
				<canvas ref={this.chartRef}></canvas>
			</div>
		)
	}
	
	public createChart(ctx: HTMLCanvasElement) {
		this.chart = new Chart(ctx as HTMLCanvasElement, {
			type: this.props.type,
			options: {
				onClick: (data, chart) => {
					if (chart.length) {
						this.props.onSelected(chart[0]['_index']);
					}
				}
			},
			data: {
				labels: this.props.labels,
				datasets: this.props.datasets
			}
		});
	}
	
	public updateChart(ctx: HTMLCanvasElement) {
		this.chart.data.datasets = this.props.datasets;
		this.chart.update(0);
	}
}




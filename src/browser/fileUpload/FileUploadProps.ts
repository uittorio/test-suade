import { CsvParser } from "../../core/csv/CsvParser";
import { Csv } from "../../core/csv/Csv";

export interface FileUploadProps {
	parser: CsvParser;
	onUpload(file: Csv): void;
}
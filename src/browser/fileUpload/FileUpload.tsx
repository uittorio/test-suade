import * as React from "react";
import './FileUpload.scss';
import { Csv } from "../../core/csv/Csv";
import { FileUploadProps } from "./FileUploadProps";

export class FileUpload extends React.Component<FileUploadProps> {
	public render() {
		return (
			<div>
				<label htmlFor="input-file" className="FileUpload-label">Upload file</label>
				<input type="file" id="input-file" className="FileUpload-input" accept="text/csv" onChange={e => this.fileUploaded(e.target.files[0])}/>
			</div>
		)
	}
	
	public fileUploaded(file: File): void {
		this.props.parser.parse(file).then((file: Csv) => {
			this.props.onUpload(file);
		})
	}
}



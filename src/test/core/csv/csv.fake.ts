import { Csv } from "../../../core/csv/Csv";
import { TableHeader } from "../../../core/table/TableHeader";
import { TableRowConfig } from "../../../core/table/TableRowConfig";

export class CsvFake implements Csv {
	public updateRow(rowIndex: number, rowKey: string, newValue: string): void {}
	public headerAsArray(): Array<TableHeader> {
		return [];
	}
	
	
	public header(): { [p: string]: string } {
		return {};
	}
	
	public row(): Array<{ [p: string]: string }> {
		return [];
	}
	
	public rowAsArray(): Array<Array<TableRowConfig>> {
		return [[]];
	}
	
}
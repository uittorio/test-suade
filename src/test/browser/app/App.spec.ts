import App from "../../../browser/app/App";
import * as React from "react";
import { SInjector } from "../../../core/injector/injector";
jest.mock("../../../core/injector/injector");

const ReactTestRenderer = require('react-test-renderer');

const injector: SInjector = new SInjector();
const reactRendered = ReactTestRenderer.create(React.createElement(App, { injector: injector}));

describe("when app is created", () => {
	it('should pass the csv parser to the fileUploader', () => {
		expect(reactRendered.toJSON()).toMatchSnapshot();
		expect(injector.get).toHaveBeenCalledWith('csvParser');
	});
});
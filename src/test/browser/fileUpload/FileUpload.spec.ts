import * as React from "react";
import { FileUpload } from "../../../browser/fileUpload/FileUpload";

const csvParser = {
	parse: jest.fn(() => Promise.resolve('data'))
};

const onUpload = jest.fn();

const ReactTestRenderer = require('react-test-renderer');

const reactRendered = ReactTestRenderer.create(React.createElement(FileUpload, { parser: csvParser, onUpload: onUpload}));

describe("when a file is uploaded", () => {
	it('should parse the file', async() => {
		await reactRendered.getInstance().fileUploaded('file');
		expect(onUpload).toHaveBeenCalledWith('data');
	});
});
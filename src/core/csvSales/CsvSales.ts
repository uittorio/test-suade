import { Csv } from "../csv/Csv";
import { TableHeader } from "../table/TableHeader";
import { TableRowConfig } from "../table/TableRowConfig";

export class CsvSales implements Csv {
	public headerAsArray(): Array<TableHeader> {
		return this._csv.headerAsArray();
	}
	
	public header(): { [p: string]: string } {
		return this._csv.header();
	}
	
	public row(): Array<{ [p: string]: string }> {
		return this._csv.row();
	}
	
	public rowAsArray(): Array<Array<TableRowConfig>> {
		return this._csv.rowAsArray().map((rows: Array<TableRowConfig>) => {
			return rows.map((row: TableRowConfig) => {
				const isEditable: boolean = row.key === 'In Millions';
				
				return {
					key: row.key,
					value: row.value,
					editable: isEditable
				}
			});
		});
	}
	
	public updateRow(rowIndex: number, rowKey: string, newValue: string): void {
		this._csv.updateRow(rowIndex, rowKey, newValue);
	}
	
	private _csv: Csv;
	
	constructor(csv: Csv) {
		this._csv = csv;
	}
}
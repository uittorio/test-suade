export interface TableHeader {
	key: string;
	value: string;
}
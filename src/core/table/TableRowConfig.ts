export interface TableRowConfig {
	key: string;
	value: string;
	editable: boolean;
}
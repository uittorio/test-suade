interface Injector {
	add(key: string, injector: any): void;
	get(key: string): any;
}
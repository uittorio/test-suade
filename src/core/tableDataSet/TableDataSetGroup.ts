export interface TableDataSetGroup<T> {
	label: string;
	values: {[key: string]: T };
}
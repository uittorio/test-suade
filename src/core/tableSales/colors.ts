export const backgroundColor: {[key: string]: string} = {
	"Europe": 'rgba(44,234,213, 0.4)',
	"Asia ": 'rgba(221,181,118, 0.4)',
	"North America": 'rgba(66,18,92, 0.4)',
	"South America": 'rgba(232,69,76, 0.4)',
	"Australia": 'rgba(67,28,255, 0.4)',
	"Africa": 'rgba(76,34,10, 0.4)'
};
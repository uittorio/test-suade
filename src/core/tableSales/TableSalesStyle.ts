import { backgroundColor } from "./colors";

export class TableSalesStyle {
	public static getBackgroundColor(type: string): string {
		return backgroundColor[type];
	}
	
	public static getAllBackgroundColor(): Array<string> {
		return [
			backgroundColor['Europe'],
			backgroundColor['Asia '],
			backgroundColor['North America'],
			backgroundColor['South America'],
			backgroundColor['Australia'],
			backgroundColor['Africa']
		]
	}
}
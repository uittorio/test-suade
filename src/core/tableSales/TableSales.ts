import { Csv } from "../csv/Csv";
import { TableDataSetGroup } from "../tableDataSet/TableDataSetGroup";

export class TableSales {
	private _data: Csv;
	
	constructor(data: Csv) {
		this._data = data;
	}
	
	public getYearsAmount(): {[key: string]: number} {
		const years: {[key: string]: number} = {};
		
		this._data.row().forEach((rows) => {
			if (years[rows['Year']]) {
				years[rows['Year']] += this._convertDollarToNumber(rows['In Millions']);
			} else {
				years[rows['Year']] = this._convertDollarToNumber(rows['In Millions']);
			}
		});
		
		return years;
	}
	
	public getContinentAmount(): {[key: string]: number}  {
		const years: {[key: string]: number} = {};
		
		this._data.row().forEach((rows) => {
			if (years[rows['Continent']]) {
				years[rows['Continent']] += this._convertDollarToNumber(rows['In Millions']);
			} else {
				years[rows['Continent']] = this._convertDollarToNumber(rows['In Millions']);
			}
		});
		
		return years;
	}
	
	public getYearsByContinentValues(): Array<TableDataSetGroup<number>> {
		const continents: Array<TableDataSetGroup<number>> = [];
		
		this._data.row().forEach((rows) => {
			const rowContinent: string = rows['Continent'];
			const rowYear: string = rows['Year'];
			const rowMillions: number = this._convertDollarToNumber(rows['In Millions']);
			
			const existingContinent: TableDataSetGroup<number> = continents.find((continent: TableDataSetGroup<number>) => {
				return continent.label == rowContinent;
			});
			
			if (existingContinent) {
				if (existingContinent.values[rowYear]) {
					existingContinent.values[rowYear] += rowMillions;
				} else {
					existingContinent.values[rowYear] = rowMillions;
				}
			} else {
				continents.push({
					label: rowContinent,
					values: {
						[rowYear]: rowMillions
					}
				})
			}
		});
		
		return continents;
	}
	
	public filterByContinent(index: number) {
	
	}
	
	private _convertDollarToNumber(string: string): number {
		return parseInt(string.replace('$', ''));
	}
}
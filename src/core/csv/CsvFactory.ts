import { Csv } from "./Csv";

export interface CsvFactory<TData> {
	create(data: TData): Csv;
}
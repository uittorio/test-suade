import { CsvData } from "./CsvData";
import { Csv } from "./Csv";

export interface CsvParser {
	parse(data: CsvData): Promise<Csv>;
}
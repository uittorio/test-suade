import { TableHeader } from "../table/TableHeader";
import { TableRowConfig } from "../table/TableRowConfig";

export interface Csv {
	headerAsArray(): Array<TableHeader>;
	header(): { [key: string]: string };
	row(): Array<{ [key: string]: string }>;
	rowAsArray(): Array<Array<TableRowConfig>>;
	updateRow(rowIndex: number, rowKey: string, newValue: string): void;
}
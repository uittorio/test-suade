const webpack = require('webpack'),
  root = require('./helpers/root'),
  webpackMerge = require('webpack-merge'),
  commonConfig = require('./webpack.common.config.js');

module.exports = webpackMerge(commonConfig, {
  mode: 'production',
  output: {
    path: root('build'),
    filename: '[name].[chunkhash].bundle.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      htmlLoader: {
        minimize: false
      }
    }),
    new webpack.NoEmitOnErrorsPlugin()
  ]
});
